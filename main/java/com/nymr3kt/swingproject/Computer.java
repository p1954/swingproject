/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.swingproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author nymr3kt
 */
public class Computer {
    private int hand ;
    private int handPlayer ;
    private int win , lose , draw ;
    private int status ;
    
    public Computer (){
    }
    private int choob(){
        return ThreadLocalRandom.current().nextInt(0, 3);
    }
    
    
    public int paoYingChoob(int handPlayer){
        this.handPlayer = handPlayer ;
        this.hand = choob() ;
        if(this.handPlayer == this.hand){
            draw++ ;
            status = 0;
            return 0 ;
        }
        if(this.handPlayer == 0 && this.hand == 1){
            win++ ;
            status = 1 ;
            return 1 ;
        }
         if(this.handPlayer == 1 && this.hand == 2){
             win++ ;
             status = 1 ;
            return 1 ;
        }
         if(this.handPlayer == 2 && this.hand == 3){
             win++ ;
             status = 1 ;
            return 1 ;
        }
         lose++ ;
         status = -1 ;
        return -1 ;
    }

    public int getHand() {
        return hand;
    }

    public int getHandPlayer() {
        return handPlayer;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
    
    
}
